$(document).ready(function(){
	$('body').on('click','#print-button',function(){
		var body_content = $("body").html();
		var printable = $("#printable").clone();
		printable.show();
		$('body').empty();
		$('body').append( printable );
		window.print();
		$('body').empty();
		$('body').append( body_content );
	});
	$("#printable").hide();
});