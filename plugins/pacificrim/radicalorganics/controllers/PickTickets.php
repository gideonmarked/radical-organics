<?php namespace PacificRim\RadicalOrganics\Controllers;

use BackendMenu;
use Log;
use Redirect;
use Backend\Classes\Controller;
use PacificRim\RadicalOrganics\Models\PickTicket;
use PacificRim\RadicalOrganics\Models\PickTicketItem;
use PacificRim\RadicalOrganics\Models\ExternalPurchaseOrder;

/**
 * Pick Tickets Back-end Controller
 */
class PickTickets extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('PacificRim.RadicalOrganics', 'rowarehouse', 'picktickets');

        $this->addCss('/plugins/pacificrim/radicalorganics/assets/css/printable.css');
        $this->addJs('/plugins/pacificrim/radicalorganics/assets/js/jquery.printElement.min.js');
        $this->addJs('/plugins/pacificrim/radicalorganics/assets/js/printable.js');

        if( $this->action == "update" )
            $this->getPrintValues($this->params[0]);
    }

    private function getPrintValues($pt_id)
    {
        $pick_ticket = PickTicket::find($pt_id);
        $po = ExternalPurchaseOrder::where('po_code',$pick_ticket->po_code)->first();
        $this->vars['items'] = PickTicketItem::where('pick_ticket_id',$pt_id)->get();
        $this->vars['po_code'] = $pick_ticket->po_code;
        $this->vars['pt_number'] = 'PNRFPT0000' . $pick_ticket->id;
        $this->vars['customer_details'] = $pick_ticket->customer_details;
        $this->vars['warehouse_details'] = $pick_ticket->warehouse_details;
        $this->vars['currency'] = $po->customer->currency;
        $this->vars['customer_code'] = $po->customer->customer_code;
        $this->vars['terms'] = $po->payment_terms;
        $this->vars['status'] = $pick_ticket->status;
        $phpdate = strtotime( $pick_ticket->created_at );
        $pick_ticket_date = date( 'M d, Y', $phpdate );
        $this->vars['pick_ticket_date'] = $pick_ticket_date;
    }

    public function onPTStage()
    {
        $pt = PickTicket::find($this->params[0]);
        $pt->status = "Staged";
        $pt->save();

        return Redirect::refresh();
    }
}
