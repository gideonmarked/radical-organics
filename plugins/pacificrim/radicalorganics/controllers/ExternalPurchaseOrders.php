<?php namespace PacificRim\RadicalOrganics\Controllers;

use BackendMenu;
use Log;
use Flash;
use Redirect;
use Backend\Classes\Controller;
use PacificRim\RadicalOrganics\Models\Invoice;
use PacificRim\RadicalOrganics\Models\ExternalPurchaseOrder;
use PacificRim\RadicalOrganics\Models\PurchasedOrderItem;
use PacificRim\RadicalOrganics\Models\InvoiceItem;

/**
 * External Purchase Orders Back-end Controller
 */
class ExternalPurchaseOrders extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('PacificRim.RadicalOrganics', 'robusiness', 'externalpurchaseorders');
        if( $this->action == "update" )
            $this->getVars();
    }

    private function getVars()
    {
        $po = ExternalPurchaseOrder::find( $this->params[0] );
        if($po !== null && Invoice::where('reference_po_id',$po->po_code)->first() === null )
        {
            $this->vars['post_shipping_status'] = $po->post_shipping_status;
            $this->vars['shipping_status'] = $po->shipping_status;
        }
    }

    public function onCreateInvoice()
    {
        $po = ExternalPurchaseOrder::find( $this->params[0] );
        if($po !== null)
        {

            if( Invoice::where('reference_po_id',$this->po_code)->first() === null )
            {
                $invoice = new Invoice;
                $invoice->customer_id = $po->customer_id;
                $invoice->reference_po_id = $po->po_code;
                $invoice->invoice_date = date('Y') . '-' . date('m') . '-' . date('d');
                $invoice->total = $po->total;
                $invoice->status = 'created';
                $invoice->payment_terms = $po->payment_terms;
                $invoice->billing_to_address = $po->billing_to_address;
                $invoice->payment_date = $po->payment_date;
                $invoice->invoice_date = date('Y') . '-' . date('m') . '-' . date('d');

                $total = 0;
                $invoice->total = $total;
                $invoice->save();

                $total = 0;
                $po_items = PurchasedOrderItem::where('external_purchase_order_id',$po->id)->get();
                foreach ($po_items as $key => $po_item) {
                    $invoice_item = new InvoiceItem;
                    $invoice_item->invoice_id = $invoice->id;
                    $invoice_item->product = $po_item->product;
                    $invoice_item->quantity = $po_item->quantity;
                    $invoice_item->master_pack = $po_item->master_pack;
                    $invoice_item->unit_cost = $po_item->unit_cost;
                    $invoice_item->total_cost = $po_item->total_cost;
                    $total = $total + $po_item->total_cost;
                    $invoice_item->type = $po_item->type;
                    $invoice_item->description = $po_item->description;
                    $invoice_item->save();
                }

                $invoice->total = $total;

                $invoice->save();

                // Sets a successful message
                Flash::success('Invoice has been created successfully!');
            }

            $po->post_shipping_status = "invoiced";
            $po->save();
            return Redirect::refresh();
        }


    }
}