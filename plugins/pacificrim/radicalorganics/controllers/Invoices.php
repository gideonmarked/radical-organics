<?php namespace PacificRim\RadicalOrganics\Controllers;

use BackendMenu;
use Log;
use Redirect;
use Backend\Classes\Controller;
use PacificRim\RadicalOrganics\Models\Invoice;
use PacificRim\RadicalOrganics\Models\ExternalPurchaseOrder;
use PacificRim\RadicalOrganics\Models\InvoiceItem;

/**
 * Invoices Back-end Controller
 */
class Invoices extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('PacificRim.RadicalOrganics', 'robusiness', 'invoices');
        $this->addCss('/plugins/pacificrim/radicalorganics/assets/css/printable.css');
        $this->addJs('/plugins/pacificrim/radicalorganics/assets/js/jquery.printElement.min.js');
        $this->addJs('/plugins/pacificrim/radicalorganics/assets/js/printable.js');
        $this->addJs('/plugins/pacificrim/radicalorganics/assets/js/form.ajax.js');

        if( $this->action == "update" )
            $this->getPrintValues($this->params[0]);
    }

    private function getPrintValues($invoice_id)
    {
        $invoice = Invoice::find($invoice_id);
        $this->vars['items'] = InvoiceItem::where('invoice_id',$invoice_id)->get();
        $this->vars['invoice_code'] = $invoice['invoice_code'];
        $this->vars['po_code'] = $invoice->reference_po_id;
        $this->vars['billing_address'] = $invoice->billing_to_address->address;
        Log::info($invoice->customer->customer_name);
        $this->vars['customer'] = $invoice->customer->customer_name;
        $this->vars['currency'] = $invoice->customer->currency;
        $this->vars['customer_code'] = $invoice->customer->customer_code;
        $this->vars['terms'] = $invoice->payment_terms;
        $phpdate = strtotime( $invoice->invoice_date );
        $invoice_date = date( 'M d, Y', $phpdate );
        $phpdate = strtotime( $invoice->due_date );
        $due_date = date( 'M d, Y', $phpdate );
        $this->vars['invoice_date'] = $invoice_date;
        $this->vars['due_date'] = $due_date;
        $this->vars['invoice_status'] = $invoice['status'];
        $this->vars['downpayment_status'] = $invoice['downpayment_status'];
        $this->vars['downpayment_percentage'] = $invoice['downpayment_percentage'];
    }

    public function onIssueInvoice()
    {
        $invoice = Invoice::find($this->params[0]);
        $invoice->status = 'issued';
        $invoice->save();

        $po = ExternalPurchaseOrder::where( 'po_code', $invoice->reference_po_id )->first();
        if($invoice->downpayment_status == "downpayment") {
            $po->post_shipping_status = 'dpissued';
        } else if($invoice->payment_terms == "balance") {
            $po->post_shipping_status = 'bissued';
        } else {
            $po->post_shipping_status = 'issued';
        }
        $po->save();

        return Redirect::refresh();
    }

    public function onPaymentInvoice()
    {
        $invoice = Invoice::find($this->params[0]);
        $invoice->status = 'paid';
        $invoice->payment_date = date('Y-m-d');
        $invoice->save();

        $po = ExternalPurchaseOrder::where( 'po_code', $invoice->reference_po_id )->first();
        if($invoice->downpayment_status == "downpayment") {
            $po->post_shipping_status = 'dppaid';
        } else if($invoice->payment_terms == "balance") {
            $po->post_shipping_status = 'bpaid';
        } else {
            $po->post_shipping_status = 'paid';
        }
        $po->save();

        return Redirect::refresh();
    }
}