<?php namespace PacificRim\RadicalOrganics\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use PacificRim\RadicalOrganics\Models\InternalPurchaseOrder;
use PacificRim\RadicalOrganics\Models\IssuePurchaseOrderItem;

/**
 * Internal Purchase Orders Back-end Controller
 */
class InternalPurchaseOrders extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('PacificRim.RadicalOrganics', 'robusiness', 'internalpurchaseorders');
        $this->addCss('/plugins/pacificrim/radicalorganics/assets/css/printable.css');
        $this->addJs('/plugins/pacificrim/radicalorganics/assets/js/jquery.printElement.min.js');
        $this->addJs('/plugins/pacificrim/radicalorganics/assets/js/printable.js');

        if( $this->action == "update" )
            $this->getPrintValues($this->params[0]);
    }

    public function onRefreshValues()
    {
        $this->getPrintValues($this->params[0]);
    }

    private function getPrintValues($po_id)
    {
        $po = InternalPurchaseOrder::find($po_id);
        $this->vars['items'] = IssuePurchaseOrderItem::where('internal_purchase_order_id',$po->id)->get();
        $this->vars['po_code'] = $po['po_code'];
        $this->vars['billing_address'] = $po->bill_to_address;
        $this->vars['shipped_from_address'] = $po->shipped_from_address;
        $this->vars['supplier'] = $po->supplier->supplier_name;
        $this->vars['currency'] = 'PHP';
        $this->vars['supplier_code'] = $po->supplier->supplier_code;
        $this->vars['issuer_name'] = $po['issuer_name'];
        $this->vars['issuer_email'] = $po['issuer_email'];
        $this->vars['issuer_contact_no'] = $po->supplier->issuer_contact_no;
        $this->vars['terms'] = $po->payment_terms;
        $phpdate = strtotime( $po['issue_date'] );
        $po_date = date( 'M d, Y', $phpdate );
        $this->vars['po_date'] = $po_date;
        $this->vars['po_status'] = $po['status'];
        $this->vars['freight_terms'] = $po['freight_terms'];
        $this->vars['downpayment_status'] = $po['downpayment_status'];
        $this->vars['downpayment_percentage'] = $po['downpayment_percentage'];
    }
}