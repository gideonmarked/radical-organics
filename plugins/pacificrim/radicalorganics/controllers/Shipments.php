<?php namespace PacificRim\RadicalOrganics\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Shipments Back-end Controller
 */
class Shipments extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('PacificRim.RadicalOrganics', 'rowarehouse', 'shipments');
    }
}