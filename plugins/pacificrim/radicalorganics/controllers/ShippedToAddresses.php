<?php namespace PacificRim\RadicalOrganics\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Shipped To Addresses Back-end Controller
 */
class ShippedToAddresses extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('PacificRim.RadicalOrganics', 'robusiness', 'shippedtoaddresses');
    }

    public function scopeUniqueSTA($query)
    {
        // return $query->where('id', 1)->get();
    }
}
