<?php namespace PacificRim\RadicalOrganics\Models;

use Model;
use Event;
use Log;
use PacificRim\RadicalOrganics\Models\ExternalPurchaseOrder;
use PacificRim\RadicalOrganics\Models\PurchasedOrderItem;
use PacificRim\RadicalOrganics\Models\InvoiceItem;

/**
 * Invoice Model
 */
class Invoice extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pacificrim_radicalorganics_invoices';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    private $rules = [
        'invoice_date' => 'required',
        'status' => 'required',
        'payment_terms' => 'required',
        'aging' => 'required',
        'instructions' => 'required',
    ];

    public $customMessages = [
        'invoice_date.required' => 'The Invoice Date is Required',
        'status.required' => 'The Status is Required',
        'payment_terms.required' => 'The Payment Term is Required',
        'aging.required' => 'The Aging is Required',
        'instructions.required' => 'The Instructions is Required',
    ];
    

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [
        
    ];
    public $hasMany = [
        'invoice_item' => 'PacificRim\RadicalOrganics\Models\InvoiceItem'
    ];
    public $belongsTo = [
        'customer' => ['PacificRim\RadicalOrganics\Models\Customer','key'=>'customer_id'],
        'billing_to_address' => ['PacificRim\RadicalOrganics\Models\BillingToAddress'],
        'shipping_to_address' => ['PacificRim\RadicalOrganics\Models\ShippedToAddress']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'attachment' => ['System\Models\File']
    ];
    public $attachMany = [];

    public function beforeCreate()
    {
        $this->invoice_code = $this->generateInvoiceCode();
    }

    public function afterCreate()
    {
        // $poid = ExternalPurchaseOrder::where('po_code',$this->reference_po_id)->first()['id'];
        // $po_items = PurchasedOrderItem::where('external_purchase_order_id',$poid)->get();
        // if($po_items === null) {
        //     $po_items = PurchasedOrderItem::where('external_purchase_order_id',null)->get();
        // }
        // foreach ($po_items as $key => $po_item) {
        //     $invoice_item = new InvoiceItem;
        //     $invoice_item->invoice_id = $this->id;
        //     $invoice_item->product = $po_item->product;
        //     $invoice_item->quantity = $po_item->quantity;
        //     $invoice_item->master_pack = $po_item->master_pack;
        //     $invoice_item->unit_cost = $po_item->unit_cost;
        //     $invoice_item->total_cost = $po_item->total_cost;
        //     $invoice_item->type = $po_item->type;
        //     $invoice_item->description = $po_item->description;
        //     $invoice_item->save();
        // }
    }

    private static function generateInvoiceCode()
    {
        $code = "PRNFI-";
        $alphanum = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        for ($i=0; $i < 5; $i++) { 
            $code = $code . substr($alphanum, rand(0,strlen($alphanum) - 1), 1);
        }

        if( self::where('invoice_code',$code)->first() ) {
            $code = self::generateInvoiceCode();
        }

        return $code;
    }

    public function filterFields($fields, $context = null)
    {
        if (!empty($fields->invoice_date->value))
        {
            $now = ( empty($fields->payment_date->value) ? time() : strtotime($fields->payment_date->value));
            $your_date = strtotime( $this->invoice_date );
            $datediff = $now - $your_date;
            $fields->aging->value = floor($datediff / (60 * 60 * 24));
        }
    }

}