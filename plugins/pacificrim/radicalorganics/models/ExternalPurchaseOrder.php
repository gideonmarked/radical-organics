<?php namespace PacificRim\RadicalOrganics\Models;

use Model;
use Log;
use Flash;
use Redirect;
use Backend;
use Session;
use PacificRim\RadicalOrganics\Models\Invoice;
use PacificRim\RadicalOrganics\Models\PickTicket;
use PacificRim\RadicalOrganics\Models\Shipment;
use PacificRim\RadicalOrganics\Models\PickTicketItem;
use PacificRim\RadicalOrganics\Models\PurchasedOrderItem;
use PacificRim\RadicalOrganics\Models\ShippedToAddress;
use PacificRim\RadicalOrganics\Models\BillingToAddress;

/**
 * ExternalPurchaseOrder Model
 */
class ExternalPurchaseOrder extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pacificrim_radicalorganics_external_purchase_orders';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    private $rules = [
        'request_date' => 'required',
        'shipping_status' => 'required',
        'payment_terms' => 'required',
        'notes' => 'required',
    ];

    public $customMessages = [
        'request_date.required' => 'The Request Date is Required',
        'shipping_status.required' => 'The Shipping Status is Required',
        'payment_terms.required' => 'The Payment Term is Required',
        'notes.required' => 'The Note is Required',
    ];
    

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [
        
    ];
    public $hasMany = [
        'purchase_order_items' => 'PacificRim\RadicalOrganics\Models\PurchasedOrderItem',
    ];
    public $belongsTo = [
        'customer' => ['PacificRim\RadicalOrganics\Models\Customer','key'=>'customer_id'],
        'billing_to_address' => ['PacificRim\RadicalOrganics\Models\BillingToAddress'],
        'shipping_to_address' => [
                                    'PacificRim\RadicalOrganics\Models\ShippedToAddress'
                                ],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [
        'attachments' => ['System\Models\File']
    ];

    public function beforeSave()
    {
        if($this->payment_terms == "Downpayment/Balance at Shipment") {
            $this->downpayment_status = "downpayment";
        }
        // return false;
    }

    public function scopeIsSTA()
    {
        return $query->where('id',1);
    }

    public function filterFields($fields, $context = null)
    {
        if(!empty($this->payment_terms))
        {
            $payment_terms = $this->payment_terms;
            if($payment_terms == 'Others'){
                $fields->other_terms->hidden = false;
                $fields->downpayment_percentage->hidden = true;  
            } else if($payment_terms == 'Downpayment/Balance at Shipment'){
                $fields->downpayment_percentage->hidden = false;
                $fields->other_terms->hidden = true;
            } else {
                $fields->other_terms->hidden = true;
                $fields->downpayment_percentage->hidden = true;
            }
        }

        if(!empty($this->post_shipping_status))
        {
            $fields->post_shipping_status->hidden = false;
        }
        if (!empty($this->customer->value))
        {
            // $product = $this->product;

            // $fields->description->value = $product->description;
            // $fields->unit_cost->value = $product->unit_cost;
        }
    }

    public function getShippingToAddressIdOptions()
    {
        if(empty($this->customer) || is_null($this->customer))
        {
            return [];
        } else {
            Session::put('customer',$this->customer);
            $list = array();
            $ships = ShippedToAddress::where('customer_id',$this->customer->id)->get();

            foreach ($ships as $ship) {
                Log::info($ship);
                $list[$ship['id']] = $ship['deliver_to_address'];
            }
            return $list;
        }
    }

    public function getBillingToAddressIdOptions()
    {
        if(empty($this->customer) || is_null($this->customer))
        {
            return [];
        } else {
            $list = array();
            $bills = BillingToAddress::where('customer_id',$this->customer->id)->get();

            foreach ($bills as $bill) {
                Log::info($bill);
                $list[$bill['id']] = $bill['address'];
            }
            return $list;
        }
    }

    public function afterCreate()
    {
        if($this->payment_terms == "Downpayment/Balance at Shipment") {
            $this->createDPInvoice();
        }

        if($this->payment_terms == "Prepaid") {
            $this->createPrepaidInvoice();
        }
    }


    public function afterSave()
    {
        if($this->shipping_status == "approved")
        {
            $this->createPickTicket();
        }
    }

    private function createDPInvoice()
    {
        if( Invoice::where('reference_po_id',$this->po_code)->where("payment_terms","Downpayment/Balance at Shipment")->first() === null)
        {
            $invoice = new Invoice;
            $invoice->customer_id = $this->customer_id;
            $invoice->billing_to_address_id = $this->billing_to_address_id;
            $invoice->shipping_to_address_id = $this->shipping_to_address_id;
            $invoice->reference_po_id = $this->po_code;
            $invoice->invoice_date = date('Y') . '-' . date('m') . '-' . date('d');
            $invoice->total = $this->total;
            $invoice->status = 'created';
            $invoice->payment_terms = $this->payment_terms;
            $invoice->billing_to_address = $this->billing_to_address;
            $invoice->shipping_to_address = $this->shipping_to_address;
            $invoice->payment_terms = $this->payment_terms;
            $invoice->downpayment_status = "downpayment";
            if($this->downpayment_status == "downpayment") {
                $invoice->downpayment_percentage = $this->downpayment_percentage * 0.01;
            }
            $invoice->save();

            // Sets a successful message
            Flash::success('Invoice has been created successfully!');
        }
    }

    private function createPrepaidInvoice()
    {
        if( Invoice::where('reference_po_id',$this->po_code)->where("payment_terms","Prepaid")->first() === null)
        {
            $invoice = new Invoice;
            $invoice->customer_id = $this->customer_id;
            $invoice->reference_po_id = $this->po_code;
            $invoice->invoice_date = date('Y') . '-' . date('m') . '-' . date('d');
            $invoice->due_date = date('Y') . '-' . date('m') . '-' . date('d');
            $invoice->total = $this->total;
            $invoice->status = 'created';
            $invoice->payment_terms = $this->payment_terms;
            $invoice->billing_to_address = $this->billing_to_address;
            $invoice->shipping_to_address_id = $this->shipping_to_address_id;
            $invoice->payment_date = $this->payment_date;

            $total = 0;
            $invoice->total = $total;
            $invoice->save();

            // Sets a successful message
            Flash::success('Invoice has been created successfully!');
        }
    }

    private function createPickTicket()
    {
        if( PickTicket::where('po_code',$this->po_code)->first() === null )
        {
            $pick_ticket = new PickTicket;
            $pick_ticket->po_code = $this->po_code;

            $customer_details = '<p>' . $this->customer->customer_name . '</p>';
            $customer_details .= '<p>' . $this->shipping_to_address->deliver_to_address . '</p>';

            $pick_ticket->customer_details = $customer_details;

            $warehouse_details = '<p>Pacific Rim - Radical Organics</p>';
            $warehouse_details .= '<p>Unit 2703 Entrata Urban Complex,</p>';
            $warehouse_details .= '<p>2609 Civic Drive,</p>';
            $warehouse_details .= '<p>Filinvest City,</p>';
            $warehouse_details .= '<p>Alabang,</p>';
            $warehouse_details .= '<p>Muntinlupa City, Philippines 1770</p>';
            
            $pick_ticket->warehouse_details = $warehouse_details;
            $pick_ticket->status = "Approved";

            $po_items = PurchasedOrderItem::where('external_purchase_order_id',$this->id)->get();

            $pick_ticket->save();

            foreach ($po_items as $key => $po_item) {
                $pick_ticket_item = new PickTicketItem;
                $pick_ticket_item->pick_ticket_id = $pick_ticket->id;
                $pick_ticket_item->product_id = $po_item->product_id;
                $pick_ticket_item->part_no = $po_item->part_no;
                $pick_ticket_item->quantity = $po_item->quantity;
                $pick_ticket_item->master_pack = $po_item->master_pack;
                $pick_ticket_item->unit_cost = $po_item->unit_cost;
                $pick_ticket_item->total_cost = $po_item->total_cost;
                $pick_ticket_item->type = $po_item->type;
                $pick_ticket_item->description = $po_item->description;
                $pick_ticket_item->save();
            }
        }         
    }

    private static function generateShipmentCode()
    {
        $code = "SNO-";
        $alphanum = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        for ($i=0; $i < 5; $i++) { 
            $code = $code . substr($alphanum, rand(0,strlen($alphanum) - 1), 1);
        }

        if( Shipment::where('shipment_no',$code)->first() ) {
            $code = self::generateShipmentCode();
        }

        return $code;
    }

}