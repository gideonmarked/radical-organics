<?php namespace PacificRim\RadicalOrganics\Models;

use Model;

/**
 * InvoiceItem Model
 */
class InvoiceItem extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pacificrim_radicalorganics_invoice_items';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    private $rules = [
        'part_no' => 'required',
        'quantity' => 'required',
        'master_pack' => 'required',
        'type' => 'required',
        'description' => 'required',
        'unit_cost' => 'required',
        'total_cost' => 'required',
        'total_po_cost' => 'required',
    ];

    public $customMessages = [
        'part_no.required' => 'The Part No. is Required',
        'quantity.required' => 'The Quantity is Required',
        'master_pack.required' => 'The Master Pack is Required',
        'type.required' => 'The Type is Required',
        'description.required' => 'The Description is Required',
        'unit_cost.required' => 'The Unit Cost is Required',
        'total_cost.required' => 'The Total Cost is Required',
        'total_po_cost.required' => 'The Total PO Cost is Required',
    ];
   

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'product' => 'PacificRim\RadicalOrganics\Models\Product'
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    

}