<?php namespace PacificRim\RadicalOrganics\Models;

use Model;
use PacificRim\RadicalOrganics\Models\ExternalPurchaseOrder;
use PacificRim\RadicalOrganics\Models\PurchasedOrderItem;
use PacificRim\RadicalOrganics\Models\InvoiceItem;
use PacificRim\RadicalOrganics\Models\Invoice;
use PacificRim\RadicalOrganics\Models\PickTicket;
use PacificRim\RadicalOrganics\Models\PickTicketItem;
use PacificRim\RadicalOrganics\Models\Product;
use PacificRim\RadicalOrganics\Models\Customer;
use Log;
use Session;

/**
 * PurchasedOrderItem Model
 */
class PurchasedOrderItem extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pacificrim_radicalorganics_purchased_order_items';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

     private $rules = [
        'part_no' => 'required',
        'quantity' => 'required',
        'master_pack' => 'required',
        'type' => 'required',
        'description' => 'required',
        'unit_cost' => 'required',
        'total_cost' => 'required',
        'total_po_cost' => 'required',
    ];

    public $customMessages = [
        'part_no.required' => 'The Part No. is Required',
        'quantity.required' => 'The Quantity is Required',
        'master_pack.required' => 'The Master Pack is Required',
        'type.required' => 'The Type is Required',
        'description.required' => 'The Description is Required',
        'unit_cost.required' => 'The Unit Cost is Required',
        'total_cost.required' => 'The Total Cost is Required',
        'total_po_cost.required' => 'The Total PO Cost is Required',
    ];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'product' => 'PacificRim\RadicalOrganics\Models\Product'
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function beforeCreate()
    {
        $customer = Session::get('customer');
        if( $customer !== null )
        {
            $customer_currency = $customer['currency'];
            
            if ( substr($this->unit_cost, 0,3) == "USD" || substr($this->unit_cost, 0,3) == "PHP" ) {
                $unit_cost = substr($this->unit_cost, 4);
            } else {
                $unit_cost = $this->unit_cost;
            }
            $this->unit_cost = $customer_currency . ' ' . number_format( $unit_cost,2,".",",");
            
            if ( substr($this->total_cost, 0,3) == "USD" || substr($this->total_cost, 0,3) == "PHP" ) {
                $total_cost = substr($this->total_cost, 4);
            } else {
                $total_cost = $customer_currency . $this->total_cost;
            }
            $this->total_cost = $this->total_cost;
        }
    }

    public function beforeUpdate()
    {
        $customer = Session::get('customer');

        $poid = $this->external_purchase_order_id;
        $cid = ExternalPurchaseOrder::find($poid)['customer_id'];
        
        $customer_currency = Customer::find($cid)['currency'] !== null ? Customer::find($cid)['currency'] : $customer = $customer['currency'];
        
        if ( substr($this->unit_cost, 0,3) == "USD" || substr($this->unit_cost, 0,3) == "PHP" ) {
            $unit_cost = substr($this->unit_cost, 4);
        } else {
            $unit_cost = $this->unit_cost;
        }
        $this->unit_cost = $customer_currency . ' ' . number_format( $unit_cost,2,".",",");
        if ( substr($this->total_cost, 0,3) == "USD" || substr($this->total_cost, 0,3) == "PHP" ) {
                $total_cost = substr($this->total_cost, 4);
        } else {
            $total_cost = $customer_currency . $this->total_cost;
        }
        $this->total_cost = $this->total_cost;
    }

    public function afterUpdate()
    {
        $po = ExternalPurchaseOrder::find($this->external_purchase_order_id);
        if($po !== null)
        {
            $invoice = Invoice::where('reference_po_id',$po->po_code)->first();

            if($po->payment_terms == "Prepaid" || $po->payment_terms == "Downpayment/Balance at Shipment") {
                $invoice_item = new InvoiceItem;
                $invoice_item->invoice_id = $invoice->id;
                $invoice_item->product = $this->product;
                $invoice_item->quantity = $this->quantity;
                $invoice_item->master_pack = $this->master_pack;
                $invoice_item->unit_cost = $this->unit_cost;
                $invoice_item->total_cost = $this->total_cost;
                $invoice_item->type = $this->type;
                $invoice_item->description = $this->description;
                $invoice_item->save();
            }
        }
    }

    public function beforeFetch() {
        $poid = $this->external_purchase_order_id;
        $cid = ExternalPurchaseOrder::find($poid)['customer_id'];
        $customer_currency = Customer::find($cid)['currency'] !== null ? Customer::find($cid)['currency'] : 'USD';
        if ( substr($this->unit_cost, 0,3) == "USD" || substr($this->unit_cost, 0,3) == "PHP" ) {
            $unit_cost = substr($this->unit_cost, 4);
        } else {
            $unit_cost = $this->unit_cost;
        }
        $this->unit_cost = $customer_currency . ' ' . number_format( $unit_cost,2,".",",");
    }

    public function filterFields($fields, $context = null)
    {
        if (!empty($fields->product->value))
        {
            $product = $this->product;

            $fields->description->value = $product->description;
            $fields->unit_cost->value = $product->unit_cost;
        }

        if (!empty($fields->quantity->value))
        {
            $customer = Session::get('customer');
            $quantity = $this->quantity;
            $poid = $this->external_purchase_order_id;
            $cid = ExternalPurchaseOrder::find($poid)['customer_id'];
            $customer_currency = Customer::find($cid)['currency'] !== null ? Customer::find($cid)['currency'] : $customer = $customer['currency'];
            $unit_cost = 0;
            if ( substr($this->unit_cost, 0,3) == "USD" || substr($this->unit_cost, 0,3) == "PHP" ) {
                $unit_cost = substr($this->unit_cost, 4);
            } else {
                $unit_cost = $this->unit_cost;
            }
            $fields->unit_cost->value = $customer_currency . ' ' . number_format( $unit_cost,2,".",",");
            $fields->total_cost->value = $customer_currency . ' ' . number_format( $unit_cost * $this->quantity,2,".",",");
            $fields->master_pack->value = $this->quantity / $this->product->master_pack ;
        }
    }

    
}