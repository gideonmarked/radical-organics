<?php namespace PacificRim\RadicalOrganics\Models;

use Model;

/**
 * BillingToAddress Model
 */
class BillingToAddress extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pacificrim_radicalorganics_billing_to_addresses';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

   private $rules = [
        'address' => 'required',
        'contact_no' => 'required',
    ];

    public $customMessages = [
        'address.required' => 'The Address is Required',
        'contact_no.required' => 'The Contact No. is Required',
    ];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'customer' => ['PacificRim\RadicalOrganics\Models\Customer']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}