<?php namespace PacificRim\RadicalOrganics\Models;

use Model;

/**
 * Supplier Model
 */
class Supplier extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pacificrim_radicalorganics_suppliers';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    private $rules = [
        'supplier_name' => 'required',
        'description' => 'required',
        'contact_no' => 'required',
    ];

    public $customMessages = [
        'supplier_name.required' => 'The Supplier Name is Required',
        'description.required' => 'The Description is Required',
        'contact_no.required' => 'The Contact No. is Required',
    ];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];


    public function beforeCreate()
    {
        $this->supplier_code = self::GenerateSupplierCode();
    }

    private static function GenerateSupplierCode()
    {
        $code = "S-";
        $alphanum = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        for ($i=0; $i < 3; $i++) { 
            $code = $code . substr($alphanum, rand(0,strlen($alphanum) - 1), 1);
        }

        if( self::where('supplier_code',$code)->first() ) {
            $code = self::GenerateSupplierCode();
        }

        return $code;
    }

}