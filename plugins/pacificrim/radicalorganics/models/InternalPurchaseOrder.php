<?php namespace PacificRim\RadicalOrganics\Models;

use Model;

/**
 * InternalPurchaseOrder Model
 */
class InternalPurchaseOrder extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pacificrim_radicalorganics_internal_purchase_orders';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    private $rules = [        
        'request_date' => 'required',
    ];

    public $customMessages = [        
        'request_date.required' => 'The Request Date is Required',
    ];
   

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [
        
    ];
    public $hasMany = [
        'purchase_order_item' => ['PacificRim\RadicalOrganics\Models\IssuePurchaseOrderItem']
    ];
    public $belongsTo = [
        'supplier' => 'PacificRim\RadicalOrganics\Models\Supplier',
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    
    public function beforeCreate()
    {
        $this->po_code = self::GeneratePuchaseOrderCode();
    }

    private static function GeneratePuchaseOrderCode()
    {
        $code = "PRNF-";
        $alphanum = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        for ($i=0; $i < 5; $i++) { 
            $code = $code . substr($alphanum, rand(0,strlen($alphanum) - 1), 1);
        }

        if( self::where('po_code',$code)->first() ) {
            $code = self::GeneratePuchaseOrderCode();
        }

        return $code;
    }
}