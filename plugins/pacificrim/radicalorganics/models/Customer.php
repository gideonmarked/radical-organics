<?php namespace PacificRim\RadicalOrganics\Models;

use Model;
use Log;

/**
 * Customer Model
 */
class Customer extends Model
{

    protected $primaryKey = 'id';

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pacificrim_radicalorganics_customers';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    private $rules = [
        'customer_name' => 'required',
        'address' => 'required',
        'contact_no' => 'required',
        'contact_person' => 'required',
        'email_add' => 'required|email|unique:users',
        'fax_no' => 'required',
        'payment_terms' => 'required',
        'currency' => 'required',
    ];

    public $customMessages = [
        'customer_name.required' => 'The Customer Name is Required',
        'address.required' => 'The Address is Required',
        'contact_no.required' => 'The Contact No. is Required',
        'contact_person.required' => 'The Contact Person is Required',
        'email_add.required' => 'The Email Address is Required',
        'fax_no.required' => 'The Fax No. is Required',
        'payment_terms.required' => 'The Payment Terms is Required',
        'currency.required' => 'The Currency is Required',
    ];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'billing_to_address' => ['PacificRim\RadicalOrganics\Models\BillingToAddress'], 
        'shipping_to_address' => ['PacificRim\RadicalOrganics\Models\ShippedToAddress']
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function beforeSave()
    {
        $this->customer_code = self::GenerateCustomerID();
    }

    private static function GenerateCustomerID()
    {
        $id = "CID";
        $customers = self::count();
        $id = $id . substr("000000",0, -1) . ( $customers + 1 );
        return $id;
    }
}