<?php namespace PacificRim\RadicalOrganics\Models;

use Model;

/**
 * Product Model
 */
class Product extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pacificrim_radicalorganics_products';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

     private $rules = [
        'part_no' => 'required',
        'description' => 'required',
        'size' => 'required',
        'master_pack' => 'required',        
        'unit_cost' => 'required',
    ];

    public $customMessages = [
        'part_no.required' => 'The Part No. is Required',       
        'description.required' => 'The Description is Required',
        'size.required' => 'The Size is Required',
        'master_pack.required' => 'The Master Pack is Required',        
        'unit_cost.required' => 'The Unit Cost is Required', 
    ];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'product' => 'PacificRim\RadicalOrganics\Models\Product'
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
