<?php namespace PacificRim\RadicalOrganics\Models;

use Model;

/**
 * ShipmentDetail Model
 */
class ShipmentDetail extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pacificrim_radicalorganics_shipment_details';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    private $rules = [
        'product_no' => 'required',
        'quantity' => 'required',
        'description' => 'required',
    ];

    public $customMessages = [
        'product_no.required' => 'The Product No. is Required',
        'quantity.required' => 'The Quantity is Required',
        'description.required' => 'The Description is Required',
    ];


    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}