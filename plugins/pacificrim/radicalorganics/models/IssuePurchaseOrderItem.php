<?php namespace PacificRim\RadicalOrganics\Models;

use Model;

/**
 * IssuePurchaseOrderItem Model
 */
class IssuePurchaseOrderItem extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pacificrim_radicalorganics_issue_purchase_order_items';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function filterFields($fields, $context = null)
    {
        if (!empty($fields->unit_price->value))
        {
            $fields->unit_price_total->value = number_format( $fields->unit_price->value * $this->quantity,2,".",",");
        }
    }
}
