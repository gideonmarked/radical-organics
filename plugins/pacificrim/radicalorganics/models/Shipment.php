<?php namespace PacificRim\RadicalOrganics\Models;

use Model;
use Validator;
use Input;
use Flash;
use PacificRim\RadicalOrganics\Models\Customer;
use PacificRim\RadicalOrganics\Models\Invoice;
use PacificRim\RadicalOrganics\Models\ExternalPurchaseOrder;
use PacificRim\RadicalOrganics\Models\PurchasedOrderItem;
use PacificRim\RadicalOrganics\Models\PickTicket;


/**
 * Shipment Model
 */
class Shipment extends Model
{
    use \October\Rain\Database\Traits\Validation;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pacificrim_radicalorganics_shipments';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    private $rules = [
            ];

    public $customMessages = [
        'freight_carrier.required' => 'The Freight Carrier is Required.',
        'reference_no.required' => 'The Carrier Tracking No. is Required.',
    ];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function beforeUpdate()
    {
        if($this->shipment_status == "staged")
            $this->shipment_status = "confirmed";
    }

    public function afterUpdate()
    {
        $po = ExternalPurchaseOrder::where('po_code',$this->po_code)->first();
        $po->shipping_status = 'shipped';
        $po->save();

        $pt = PickTicket::where('po_code',$this->po_code)->first();
        if($pt->status == "Staged")
        {
            $pt->status = "Confirmed";
            $pt->save();
        }


        if( $po->payment_terms == "Downpayment/Balance at Shipment" && Invoice::where('reference_po_id',$po->po_code)->where("payment_terms","Downpayment/Balance at Shipment")->where("downpayment_status","balance")->first() === null )
        {
            $invoice = new Invoice;
            $invoice->customer_id = $po->customer_id;
            $invoice->reference_po_id = $po->po_code;
            $invoice->invoice_date = date('Y') . '-' . date('m') . '-' . date('d');
            $invoice->total = $po->total;
            $invoice->status = 'created';
            $invoice->payment_terms = $po->payment_terms;
            $invoice->billing_to_address_id = $po->billing_to_address_id;
            $invoice->shipping_to_address_id = $po->shipping_to_address_id;
            $invoice->payment_date = $po->payment_date;
            $invoice->downpayment_status = "balance";
            $invoice->downpayment_percentage = $po->downpayment_percentage;
            $invoice->save();

            $po_items = PurchasedOrderItem::where('external_purchase_order_id',$po->id)->get();

            foreach ($po_items as $key => $po_item) {
                $invoice_item = new InvoiceItem;
                $invoice_item->invoice_id = $invoice->id;
                $invoice_item->product = $po_item->product;
                $invoice_item->quantity = $po_item->quantity;
                $invoice_item->master_pack = $po_item->master_pack;
                $invoice_item->unit_cost = $po_item->unit_cost;
                $invoice_item->total_cost = $po_item->total_cost;
                $invoice_item->type = $po_item->type;
                $invoice_item->description = $po_item->description;
                $invoice_item->save();
            }

            // Sets a successful message
            Flash::success('Invoice has been created successfully!');
        } else 
        {
            // $invoice = new Invoice;
            // $invoice->customer_id = $po->customer_id;
            // $invoice->reference_po_id = $po->po_code;
            // $invoice->invoice_date = date('Y') . '-' . date('m') . '-' . date('d');
            // $invoice->total = $po->total;
            // $invoice->status = 'created';
            // $invoice->payment_terms = $po->payment_terms;
            // $invoice->billing_address_id = $po->billing_address_id ? $po->billing_address_id : 1;
            // $invoice->payment_date = $po->payment_date;
            // $invoice->save();

            // $po_items = PurchasedOrderItem::where('external_purchase_order_id',$po->id)->get();

            // foreach ($po_items as $key => $po_item) {
            //     $invoice_item = new InvoiceItem;
            //     $invoice_item->invoice_id = $invoice->id;
            //     $invoice_item->product = $po_item->product;
            //     $invoice_item->quantity = $po_item->quantity;
            //     $invoice_item->master_pack = $po_item->master_pack;
            //     $invoice_item->unit_cost = $po_item->unit_cost;
            //     $invoice_item->total_cost = $po_item->total_cost;
            //     $invoice_item->type = $po_item->type;
            //     $invoice_item->description = $po_item->description;
            //     $invoice_item->save();
            // }

            // // Sets a successful message
            // Flash::success('Invoice has been created successfully!');
        }
    }
}