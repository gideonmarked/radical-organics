<?php namespace PacificRim\RadicalOrganics\Models;

use Model;
use Log;
use PacificRim\RadicalOrganics\Models\Shipment;
use PacificRim\RadicalOrganics\Models\PurchasedOrderItem;
use PacificRim\RadicalOrganics\Models\PickTicketItem;

/**
 * PickTicket Model
 */
class PickTicket extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pacificrim_radicalorganics_pick_tickets';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    private $rules = [
        'customer_details' => 'required',
        'warehouse_details' => 'required',
        'product_details' => 'required',  
    ];

    public $customMessages = [
        'customer_details.required' => 'The Customer Details is Required',
        'warehouse_details.required' => 'The Warehouse Details is Required',
        'product_details.required' => 'The Product Details is Required',
    ];


    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'pick_ticket_item' => 'PacificRim\RadicalOrganics\Models\PickTicketItem'
    ];
    public $belongsTo = [
        'po' => 'PacificRim\RadicalOrganics\Models\ExternalPurchaseOrder',
        'shipment' => 'PacificRim\RadicalOrganics\Models\Shipment',
    ];
    public $belongsToMany = [

    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function afterUpdate()
    {
        if($this->status == "Staged") {
            $this->createShipment();
        }
    }

    private function createShipment()
    {
        if( Shipment::where('po_code',$this->po_code)->first() === null )
        {
            $shipment = new Shipment;
            $shipment->shipment_no = $this->generateShipmentCode();
            $shipment->shipment_date = date('Y') . '-' . date('m') . '-' . date('d');
            $shipment->po_code = $this->po_code;
            $shipment->shipment_status = 'staged';
            $shipment->save();
        }
    }

    private static function generateShipmentCode()
    {
        $code = "SNO-";
        $alphanum = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        for ($i=0; $i < 5; $i++) { 
            $code = $code . substr($alphanum, rand(0,strlen($alphanum) - 1), 1);
        }

        if( Shipment::where('shipment_no',$code)->first() ) {
            $code = self::generateShipmentCode();
        }

        return $code;
    }
}