<?php namespace PacificRim\RadicalOrganics\Models;

use Model;

/**
 * ShippedToAddress Model
 */
class ShippedToAddress extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pacificrim_radicalorganics_shipped_to_addresses';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    private $rules = [
        'deliver_to_address' => 'required',
        'mail_to_address' => 'required',
        'contact_no' => 'required',
    ];

    public $customMessages = [
        'deliver_to_address.required' => 'The Deliver To Address is Required',
        'mail_to_address.required' => 'The Mail To Address is Required',
        'contact_no.required' => 'The Contact No. is Required',
    ];
   

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'customer' => ['PacificRim\RadicalOrganics\Models\Customer']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}