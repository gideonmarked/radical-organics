<?php namespace PacificRim\RadicalOrganics;

use Backend;
use Event;
use Log;
use System\Classes\PluginBase;
use PacificRim\RadicalOrganics\models\ExternalPurchaseOrder;
use PacificRim\RadicalOrganics\models\Customer;
use PacificRim\RadicalOrganics\models\Invoice;
use PacificRim\RadicalOrganics\models\PickTicket;

/**
 * RadicalOrganics Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'RadicalOrganics',
            'description' => 'No description provided yet...',
            'author'      => 'PacificRim',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        Event::listen('backend.page.beforeDisplay', function($controller, $action, $params) {
            if(method_exists($this, 'addCss'))
                $this->addJs('/plugins/pacificrim/radicalorganics/assets/css/backend.form.extension.css'); 
        });
    }


    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'PacificRim\RadicalOrganics\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'pacificrim.radicalorganics.*' => [
                'tab' => 'RadicalOrganics',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'robusiness' => [
                'label'       => 'Business',
                'url'         => Backend::url('pacificrim/radicalorganics/customers'),
                'icon'        => 'icon-shopping-bag',
                'permissions' => ['pacificrim.radicalorganics.*'],
                'order'       => 1,
                'sideMenu' => [
                    'customers' => [
                        'label' => 'Customers',
                        'icon'  => 'icon-users',
                        'url'   => Backend::url('pacificrim/radicalorganics/customers'),
                        'permissions' => ['pacificrim.radicalorganics.*']
                    ],

                    'externalpurchaseorders' => [
                        'label' => 'Customer Purchase Orders',
                        'icon'  => 'icon-file-text',
                        'url'   => Backend::url('pacificrim/radicalorganics/externalpurchaseorders'),
                        'permissions' => ['pacificrim.radicalorganics.*']
                    ],

                    'invoices' => [
                        'label' => 'Invoices',
                        'icon'  => 'icon-file',
                        'url'   => Backend::url('pacificrim/radicalorganics/invoices'),
                        'permissions' => ['pacificrim.radicalorganics.*']
                    ],

                    'suppliers' => [
                        'label' => 'Supplier',
                        'icon'  => 'icon-briefcase',
                        'url'   => Backend::url('pacificrim/radicalorganics/suppliers'),
                        'permissions' => ['pacificrim.radicalorganics.*']
                    ],

                    'internalpurchaseorders' => [
                        'label' => 'Issued Purchase Orders',
                        'icon'  => 'icon-file-text',
                        'url'   => Backend::url('pacificrim/radicalorganics/internalpurchaseorders'),
                        'permissions' => ['pacificrim.radicalorganics.*']
                    ],
                ],
            ],

            'rowarehouse' => [
                'label'       => 'Warehouse',
                'url'         => Backend::url('pacificrim/radicalorganics/products'),
                'icon'        => 'icon-building',
                'permissions' => ['pacificrim.radicalorganics.*'],
                'order'       => 2,
                'sideMenu' => [

                    'shipments' => [
                        'label' => 'Shipments',
                        'icon'  => 'icon-truck',
                        'url'   => Backend::url('pacificrim/radicalorganics/shipments'),
                        'permissions' => ['pacificrim.radicalorganics.*']
                    ],

                    'picktickets' => [
                        'label' => 'Pick Tickets',
                        'icon'  => 'icon-briefcase',
                        'url'   => Backend::url('pacificrim/radicalorganics/picktickets'),
                        'permissions' => ['pacificrim.radicalorganics.*']
                    ],

                    

                    'products' => [
                        'label' => 'Products',
                        'icon'  => 'icon-list',
                        'url'   => Backend::url('pacificrim/radicalorganics/products'),
                        'permissions' => ['pacificrim.radicalorganics.*']
                    ],

                    
                ],
            ],
        ];
    }

    public function registerListColumnTypes()
    {
        return [
            // A local method, i.e $this->evalCurrencyListColumn()
            'currency' => [$this, 'evalCurrencyListColumn'],
        ];
    }

    public function evalCurrencyListColumn($value, $column, $record)
    {
        $poid = -10;
        $cid = -10;
        if(isset($record->external_purchase_order_id))
        {
            $poid = $record->external_purchase_order_id;
            $cid = ExternalPurchaseOrder::find($poid)['customer_id'];
        }

        if(isset($record->external_purchase_order_id))
        {
            $poid = $record->external_purchase_order_id;
            $cid = ExternalPurchaseOrder::find($poid)['customer_id'];
        }

        if(isset($record->pick_ticket_id))
        {
            $ptid = $record->pick_ticket_id;
            $pocd = PickTicket::where('id',$ptid)->first()['po_code'];
            $cid = ExternalPurchaseOrder::where('po_code',$pocd)->first()['customer_id'];
        }
        
        $customer_currency = Customer::find($cid)['currency'];
        return $customer_currency . ' ' . number_format(substr($value, 4),2,".",",");
    }

}
