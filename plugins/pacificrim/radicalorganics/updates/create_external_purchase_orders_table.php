<?php namespace PacificRim\RadicalOrganics\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateExternalPurchaseOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('pacificrim_radicalorganics_external_purchase_orders', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('billing_to_address_id');
            $table->integer('shipping_to_address_id');
            $table->string('po_code');
            $table->dateTime('request_date');
            $table->string('shipping_status');
            $table->string('post_shipping_status')->nullable();
            $table->string('status')->nullable()->default('open');
            $table->string('payment_terms');
            $table->string('other_terms');
            $table->string('downpayment_status');
            $table->double('downpayment_percentage');
            $table->string('notes');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pacificrim_radicalorganics_external_purchase_orders');
    }
}
