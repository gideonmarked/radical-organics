<?php namespace PacificRim\RadicalOrganics\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateBillingToAddressesTable extends Migration
{
    public function up()
    {
        Schema::create('pacificrim_radicalorganics_billing_to_addresses', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('external_purchase_order_id');
            $table->string('address');
            $table->string('contact_no');
            $table->string('email');
            $table->timestamps();
        });

        Schema::create('pacificrim_radicalorganics_po_bta', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('external_purchase_order_id');
            $table->integer('billing_to_address_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pacificrim_radicalorganics_po_bta');
        Schema::dropIfExists('pacificrim_radicalorganics_billing_to_addresses');
    }
}
