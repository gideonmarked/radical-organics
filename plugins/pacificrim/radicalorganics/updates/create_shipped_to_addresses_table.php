<?php namespace PacificRim\RadicalOrganics\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateShippedToAddressesTable extends Migration
{
    public function up()
    {
        Schema::create('pacificrim_radicalorganics_shipped_to_addresses', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('external_purchase_order_id'); 
            $table->string('deliver_to_address');
            $table->string('mail_to_address');
            $table->string('email');
            $table->string('contact_no');
            $table->timestamps();
        });


        Schema::create('pacificrim_radicalorganics_po_sta', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('shipping_to_address_id');
            $table->integer('external_purchase_order_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pacificrim_radicalorganics_po_sta');
        Schema::dropIfExists('pacificrim_radicalorganics_shipped_to_addresses');
    }
}

