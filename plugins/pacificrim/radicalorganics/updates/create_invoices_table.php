<?php namespace PAcificRim\RadicalOrganics\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateInvoicesTable extends Migration
{
    public function up()
    {
        Schema::create('pacificrim_radicalorganics_invoices', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('invoice_code',12);
            $table->integer('customer_id');
            $table->integer('billing_to_address_id');
            $table->integer('shipping_to_address_id');
            $table->string('reference_po_id',12);
            $table->dateTime('invoice_date');
            $table->dateTime('due_date');
            $table->double('total')->nullable();
            $table->string('status');
            $table->string('payment_terms');
            $table->string('downpayment_status');
            $table->string('downpayment_percentage');
            $table->integer('billing_address_id');
            $table->dateTime('payment_date')->nullable();
            $table->integer('aging');
            $table->string('instructions');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pacificrim_radicalorganics_invoices');
    }
}
