<?php namespace PacificRim\RadicalOrganics\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateShipmentDetailsTable extends Migration
{
    public function up()
    {
        Schema::create('pacificrim_radicalorganics_shipment_details', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('shipment_id');
            $table->string('product_no');
            $table->integer('quantity');
            $table->string('description');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pacificrim_radicalorganics_shipment_details');
    }
}
