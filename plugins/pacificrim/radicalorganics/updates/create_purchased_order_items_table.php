<?php namespace PacificRim\RadicalOrganics\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePurchasedOrderItemsTable extends Migration
{
    public function up()
    {
        Schema::create('pacificrim_radicalorganics_purchased_order_items', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('external_purchase_order_id')->nullable();
            $table->integer('internal_purchase_order_id')->nullable();
            $table->integer('product_id');
            $table->string('part_no');
            $table->integer('quantity');
            $table->integer('master_pack');
            $table->string('type');
            $table->string('description');
            $table->string('unit_cost');
            $table->string('total_cost');
            $table->string('total_po_cost');
            $table->timestamps();
        });

        /*Schema::create('pacificrim_radicalorganics_external_purchase_orders_purchased_order_items', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('external_purchase_orders_id');
            $table->string('purchased_order_items_id');
            $table->timestamps();
        });*/
    }

    public function down()
    {
        Schema::dropIfExists('pacificrim_radicalorganics_purchased_order_items');
    }
}
