<?php namespace PacificRim\RadicalOrganics\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateIssuePurchaseOrderItemsTable extends Migration
{
    public function up()
    {
        Schema::create('pacificrim_radicalorganics_issue_purchase_order_items', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('internal_purchase_order_id')->nullable();
            $table->string('item_code');
            $table->string('item_description');
            $table->string('quantity');
            $table->string('unit_measure');
            $table->string('unit_price');
            $table->string('unit_price_total');
            $table->string('notes');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pacificrim_radicalorganics_issue_purchase_order_items');
    }
}
