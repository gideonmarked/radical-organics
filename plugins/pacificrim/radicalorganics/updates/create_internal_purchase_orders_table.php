<?php namespace PacificRim\RadicalOrganics\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateInternalPurchaseOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('pacificrim_radicalorganics_internal_purchase_orders', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('po_code');    
            $table->integer('supplier_id');
            $table->string('bill_to_address');
            $table->string('shipped_from_address');
            $table->string('freight_terms');
            $table->string('payment_terms');
            $table->dateTime('issue_date');
            $table->string('status');
            $table->string('issuer_name');
            $table->string('issuer_email');
            $table->string('issuer_contact_number');
            $table->string('notes');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pacificrim_radicalorganics_internal_purchase_orders');
    }
}
