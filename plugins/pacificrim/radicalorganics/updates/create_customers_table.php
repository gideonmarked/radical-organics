<?php namespace PacificRim\RadicalOrganics\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCustomersTable extends Migration
{
    public function up()
    {
        Schema::create('pacificrim_radicalorganics_customers', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('customer_code',9);
            $table->string('customer_name');
            $table->string('address');
            $table->string('contact_no');
            $table->string('contact_person');
            $table->string('email_add');
            $table->string('fax_no');
            $table->string('payment_terms');
            $table->string('currency');
            $table->timestamps();
        });

        /*Schema::create('pacificrim_radicalorganics_customers_shipped_to_addresses', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('customer_name');
            $table->string('address');
            $table->string('contact_no');
            $table->string('email_add');
            $table->string('fax_no');
            $table->string('payment_terms');
            $table->timestamps();
        });*/
    }

    public function down()
    {
        Schema::dropIfExists('pacificrim_radicalorganics_customers');
    }
}
