<?php namespace PacificRim\RadicalOrganics\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateShipmentsTable extends Migration
{
    public function up()
    {
        Schema::create('pacificrim_radicalorganics_shipments', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('shipment_no');
            $table->string('po_code');
            $table->string('pick_ticket_no');
            $table->date('shipment_date');
            $table->time('shipment_time');
            $table->dateTime('delivery_date')->nullable();
            $table->string('freight_carrier');
            $table->string('reference_no');
            $table->string('shipment_status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pacificrim_radicalorganics_shipments');
    }
}
