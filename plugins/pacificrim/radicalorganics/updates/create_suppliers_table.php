<?php namespace PacificRim\RadicalOrganics\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSuppliersTable extends Migration
{
    public function up()
    {
        Schema::create('pacificrim_radicalorganics_suppliers', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('supplier_code');
            $table->string('supplier_name');
            $table->string('email');
            $table->string('contact_no');
            $table->string('address');
            $table->string('description');
            $table->string('shipped_to_address');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pacificrim_radicalorganics_suppliers');
    }
}
