<?php namespace PacificRim\RadicalOrganics\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateReceiveAddressesTable extends Migration
{
    public function up()
    {
        Schema::create('pacificrim_radicalorganics_receive_addresses', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('address');
            $table->string('contact_no');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pacificrim_radicalorganics_receive_addresses');
    }
}
