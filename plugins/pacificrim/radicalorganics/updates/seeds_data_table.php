<?php namespace PacificRim\RadicalOrganics\Updates;

use Seeder;
use PacificRim\RadicalOrganics\Models\Product;
use PacificRim\RadicalOrganics\Models\Customer;
use PacificRim\RadicalOrganics\Models\Supplier;
use PacificRim\RadicalOrganics\Models\ShippedToAddress;
use PacificRim\RadicalOrganics\Models\BillingToAddress;

class SeedDataTable extends Seeder
{
    public function run()
    {

        //seed part_no
        $product = Product::create([
            'part_no'                 => 'PRCC01-8GR',
            'description'                 => 'ORGANIC COCONUT CHIPS - COCONUT SUGAR & SEA SALT (ORIGINAL FLAVOR) 8GR',
            'master_pack'               => '500'
        ]);

        $product = Product::create([
            'part_no'                 => 'PRCC01-20GR',
            'description'                 => 'ORGANIC COCONUT CHIPS - COCONUT SUGAR & SEA SALT (ORIGINAL FLAVOR) 20GR',
            'master_pack'               => '250'
        ]);

        $product = Product::create([
            'part_no'                 => 'PRCC01-40GR',
            'description'                 => 'ORGANIC COCONUT CHIPS - COCONUT SUGAR & SEA SALT (ORIGINAL FLAVOR) 40GR',
            'master_pack'               => '108'
        ]);

        $product = Product::create([
            'part_no'                 => 'PRCC01-80GR',
            'description'                 => 'ORGANIC COCONUT CHIPS - COCONUT SUGAR & SEA SALT (ORIGINAL FLAVOR) 80GR',
            'master_pack'               => '56'
        ]);

        $product = Product::create([
            'part_no'                 => 'PRCCO2-8GR',
            'description'                 => 'ORGANIC COCONUT CHIPS - SIMPLY SALTED 8GR',
            'master_pack'               => '500'
        ]);

        $product = Product::create([
            'part_no'                 => 'PRCCO2-20GR',
            'description'                 => 'ORGANIC COCONUT CHIPS - SIMPLY SALTED 20GR',
            'master_pack'               => '250'
        ]);

        $product = Product::create([
            'part_no'                 => 'PRCCO2-40GR',
            'description'                 => 'ORGANIC COCONUT CHIPS - SIMPLY SALTED 40GR',
            'master_pack'               => '108'
        ]);

        $product = Product::create([
            'part_no'                 => 'PRCCO2-80GR',
            'description'                 => 'ORGANIC COCONUT CHIPS - SIMPLY SALTED 80GR',
            'master_pack'               => '56'
        ]); 

        $product = Product::create([
            'part_no'                 => 'PRCC03-8GR',
            'description'                 => 'ORGNIC COCONUT CHIPS - CHILI LIME 8GR',
            'master_pack'               => '500'
        ]);

        $product = Product::create([
            'part_no'                 => 'PRCC03-20GR',
            'description'                 => 'ORGNIC COCONUT CHIPS - CHILI LIME 20GR',
            'master_pack'               => '250'
        ]);

        $product = Product::create([
            'part_no'                 => 'PRCC03-40GR',
            'description'                 => 'ORGNIC COCONUT CHIPS - CHILI LIME 40GR',
            'master_pack'               => '108'
        ]);

        $product = Product::create([
            'part_no'                 => 'PRCC03-80GR',
            'description'                 => 'ORGNIC COCONUT CHIPS - CHILI LIME 80GR',
            'master_pack'               => '56'
        ]);

        $product = Product::create([
            'part_no'                 => 'PRVCO-500ML',
            'description'                 => 'ORGANIC VIRGIN COCONUT OIL - 500ML',
            'master_pack'               => '24'
        ]);

        $product = Product::create([
            'part_no'                 => 'PRCS-500GR',
            'description'             => 'ORGANIC COCONUT SUGAR - 500GR',
            'master_pack'             => '24'
        ]);

        $customer = Customer::create([
            'customer_code'                 => 'CID-000001',
            'customer_name'                 => 'RSV Company, Inc.',
            'address'                       => 'Cecilia Chapman 711-2880 Nulla St. Mankato Mississippi 96522',
            'email_add'                     => 'foo@foo.com',
            'currency'                      => 'PHP'
        ]); 

        $customer = Customer::create([
            'customer_code'                 => 'CID-000002',
            'customer_name'                 => 'Duty Free Philippines Corporation',
            'address'                       => 'Fiesta Mall - EHA Building, Ninoy Avenue, Paranaque City, 1704, Philippines',
            'email_add'                     => 'compuestomi@dfp.com.ph',
            'contact_no'                    => '09209130653',
            'fax_no'                        => '02 879 3544',
            'currency'                      => 'PHP'
        ]);

        $customer = Supplier::create([
            'supplier_name'                 => 'Some Supplier',
            'email'                         => 'some_supplier@gmail.com',
            'address'                       => 'Some Sample Address',
            'description'                   => 'Ingredients',
            'contact_no'                    => '15596549845',
        ]); 

        $shippedToAddress = ShippedToAddress::create([
            'customer_id'                 => 1,
            'deliver_to_address'          => 'Celeste Slater 606-3727 Ullamcorper. Street Roseville NH 11523',
            'mail_to_address'             => 'Iris Watson  P.O. Box 283 8562 Fusce Rd. Frederick Nebraska 20620',
            'contact_no'                  => 'foo@foo.com',
        ]);

        $shippedToAddress = ShippedToAddress::create([
            'customer_id'                 => 2,
            'deliver_to_address'          => 'Fiesta Mall - EHA Building, Ninoy Avenue, Paranaque City, 1704, Philippines',
            'mail_to_address'             => 'Fiesta Mall - EHA Building, Ninoy Avenue, Paranaque City, 1704, Philippines',
            'contact_no'                  => '',
        ]);

        $billingToAddress = BillingToAddress::create([
            'customer_id'                 => 1,
            'address'          => 'Celeste Slater 606-3727 Ullamcorper. Street Roseville NH 11523',
            'contact_no'                  => 'foo@foo.com',
        ]);

        $billingToAddress = BillingToAddress::create([
            'customer_id'                 => 3,
            'address'          => 'Fiesta Mall - EHA Building, Ninoy Avenue, Paranaque City, 1704, Philippines',
            'contact_no'                  => '',
        ]);         
        
    }
}