<?php namespace PacificRim\RadicalOrganics\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePickTicketsTable extends Migration
{
    public function up()
    {
        Schema::create('pacificrim_radicalorganics_pick_tickets', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('po_code');
            $table->string('pick_ticket_no');
            $table->string('customer_details');
            $table->string('warehouse_details');
            $table->string('product_details'); 
            $table->string('status'); 
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pacificrim_radicalorganics_pick_tickets');
    }
}
